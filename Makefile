cpu	:= $(shell uname -m)
os	:= $(shell uname -s)
ver	:= 0.1.1
dst := _build/form2xhtml-$(os)-$(cpu)-$(ver).cgi
#
# https://github.com/ocaml/dune/tree/master/example/sample-projects/hello_world
# via https://stackoverflow.com/a/54712669
#
.PHONY: all build clean test install uninstall doc examples

%.rdf: %.ttl
	rapper -i turtle -o rdfxml-abbrev $< > $@

final: build $(dst) $(cdst) doap.rdf

build:
	dune build

all: build

_build/default/form2xhtml.exe: build

$(dst): _build/default/form2xhtml.exe
	cp $< $@
	chmod u+w $@
	strip $@
	ls -l $@

test:
	dune runtest

install:
	dune install

uninstall:
	dune uninstall

doc:
	dune build @doc

clean:
	rm -rf _build *.install
