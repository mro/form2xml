let err i msgs =
  let exe = Filename.basename Sys.executable_name in
  msgs |> List.cons exe |> String.concat ": " |> prerr_endline;
  i

let print_version oc =
  let exe = Filename.basename Sys.executable_name in
  Printf.fprintf oc "%s: https://mro.name/%s/v/%s+%s\n" exe "form2xhtml" Version.dune_project_version Version.git_sha;
  0

let print_help oc =
  Printf.fprintf oc {|Convert a HTTP multipart/form-data RFC 2388 POST dump into a xhtml form.

See

  * https://tools.ietf.org/html/rfc2388
  * https://ec.haxx.se/http/http-multipart
  * https://tools.ietf.org/html/rfc4287

SYNOPSIS

  $ form2xhtml -h
  $ form2xhtml -V
  $ form2xhtml enclosure_prefix < source.dump > target.html
  $ form2xhtml ./ < source.dump > target.html
  $ form2xhtml /dev/null < source.dump > target.html
|};
  0

let () =
  (match Sys.argv |> Array.to_list |> List.tl with
   | [ "-h" ] | [ "--help" ] -> print_help stdout
   | [ "-V" ] | [ "--version" ] -> print_version stdout
   | [ pre ] ->
     pre |> Rfc2388.process stdin stdout;
     0
   | _ -> err 2 [ "get help with -h" ])
  |> exit
