open Alcotest

let set_up () =
  Unix.chdir "../../"

let tc_parse0 () =
  let fn = "dumps/2020-12-02T110016.post" in
  let st = Unix.stat fn in
  check int __LOC__ 235570 st.st_size;
  let ic = open_in_gen [ Open_rdonly; Open_binary ] 0o222 fn in
  match Rfc2388.process ic stdout "/dev/null" with
  (* | Ok { Part.name = n; filename = None; mime = None } ->
     Assert2.equals_string "uhu" "title" n
  *)
  | _ -> assert true

let tc_parse1 () =
  let fn = "dumps/2025-02-11T162303.post" in
  let st = Unix.stat fn in
  check int __LOC__ 46086 st.st_size;
  let ic = open_in_gen [ Open_rdonly; Open_binary ] 0o222 fn in
  let tf,oc = Filename.open_temp_file "form2xhtml-" ".xml" in
  Rfc2388.process ic oc "/dev/null";
  oc |> close_out;
  let st' = Unix.stat tf in
  check int __LOC__ 525 st'.st_size;
  let ic = open_in tf in
  let body = st'.st_size |> really_input_string ic in
  ic |> close_in;
  Unix.unlink tf;
  body |> check string __LOC__ {|<?xml version="1.0" encoding="UTF-8"?>
<form method="post" encoding="multipart/form-data">
<textarea name="title">Test 2012</textarea>
<textarea name="m:price">6</textarea>
<textarea name="m:amount">5</textarea>
<textarea name="m:hours">4</textarea>
<textarea name="m:email">foobarbaz@example.com</textarea>
<textarea name="content">Some nasty &lt;!-- --&gt; &amp; &quot; ' &lt;![CDATA[ uhu ]]&gt; characters</textarea>
<input type="file" mime="image/jpeg" name="enclosure" value="2003-06-18-193031-fernsicht.p.jpg"/>
</form>|}
let () =
  run
    "form2xml" [
    __FILE__ , [
      "set_up"   , `Quick, set_up;
      "tc_parse0", `Quick, tc_parse0 ;
      "tc_parse1", `Quick, tc_parse1 ;
    ]
  ]
